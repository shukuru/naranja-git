"""Naranja - Multiple RSS feed manager and generator

db/models.py: Application database models classes
"""

from naranja.db import db

class Channel(db.Model):
    """RSS channel model
    """
    __tablename__ = 'rss_channel'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=True, nullable=False)
    link = db.Column(db.String, unique=True, nullable=False)
    description = db.Column(db.String, nullable=False)
    language = db.Column(db.String(8))
    copyright = db.Column(db.String)
    managing_editor = db.Column(db.String)
    webmaster = db.Column(db.String)
    last_build_date = db.Column(db.String, nullable=False)
    category = db.Column(db.String)

    items = db.relationship(
        'Item', back_populates='channel', cascade='all, delete-orphan'
    )

    def __repr__(self) -> str:
        return f"""Channel(
            id={self.id!r}, 
            title={self.title!r}, 
            description={self.description!r}, 
            last_build_date={self.last_build_date!r}
        )
        """

class Item(db.Model):
    """RSS channel item model
    """

    __tablename__ = 'rss_items'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=True, nullable=False)
    link = db.Column(db.String, unique=True)
    description = db.Column(db.String)
    author = db.Column(db.String)
    category = db.Column(db.String)
    comments = db.Column(db.String)
    guid = db.Column(db.String(256), unique=True, nullable=False)
    pub_date = db.Column(db.String, nullable=False)

    channel_id = db.Column(db.Integer, db.ForeignKey('rss_channel.id'), nullable=False)
    channel = db.relationship(
        'Channel', back_populates='items'
    )

    def __repr__(self) -> str:
        return f"""Item(
            id={self.id!r}, 
            title={self.title!r}, 
            description={self.description!r}, 
            link={self.link!r}, 
            pub_date={self.pub_date!r}
        )
        """

"""Naranja - Multiple RSS feed manager and generator

db/__init__.py: Application database related package import file
"""

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
"""Naranja - Multiple RSS feed manager and generator

__init__.py: Main application package import file
"""

import sys

from flask import Flask
import toml

from naranja.helpers import dict_keys_toupper, nowdt_to_rfc822
from naranja.db import db
from naranja.db.models import Channel

def create_app(config_filename):
    """
    """

    app = Flask(__name__)

    # Load configuration file
    try:
        config = toml.load(config_filename)
        app.config.update(
            dict_keys_toupper(config['app'])
        )
        app.logger.info(
            f"Found {len(config['channels'])} RSS channel(s) in configuration"
        )
    except (TypeError, FileNotFoundError, toml.TomlDecodeError) as error:
        app.logger.critical(
            f"Unable to load configuration file {config_filename}: {error}"
        )

        sys.exit(2)
    
    # Database configuration, set default URI to SQLite memory instance
    if not app.config['DATABASE_URI']:
        app.config.update(
            DATABASE_URI = 'sqlite:///:memory:'
        )

    # Update database configuration
    app.config.update(
        SQLALCHEMY_DATABASE_URI = app.config['DATABASE_URI'],
        SQLALCHEMY_TRACK_MODIFICATIONS = False,
        SQLALCHEMY_ECHO = True,
    )

    # Database initialization
    db.init_app(app)

    # Create database tables
    with app.app_context():
        db.create_all()

        # Populate channels table from configuration
        for _, ch in config['channels'].items():
            channel = Channel(
                title = ch['title'],
                link = ch['link'],
                description = ch['description'],
                language = ch['language'] if 'language' in ch else None,
                copyright = ch['copyright'] if 'copyright' in ch else None,
                managing_editor = ch['managing_editor'] if 'managing_editor' in ch else None,
                webmaster = ch['webmaster'] if 'webmaster' in ch else None,
                category = ch['category'] if 'category' in ch else None,
                last_build_date = nowdt_to_rfc822()
            )

            db.session.add(channel)
        
        db.session.commit()

    return app
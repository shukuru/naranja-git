"""Naranja - Multiple RSS feed manager and generator

helpers.py: Application convenience subroutines
"""

from ast import Str
from datetime import datetime
from email import utils
from typing import Dict

def dict_keys_toupper(d: Dict) -> Dict:
    upper_d = {}

    for key, value in d.items():
        upper_d[key.upper()] = value

    return upper_d

def nowdt_to_rfc822() -> str:
    return utils.format_datetime(
        datetime.now()
    )